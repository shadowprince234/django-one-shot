from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todoslist = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todoslist,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
        return redirect("todo_list_detail", list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            list.save()
        return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "list": list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    delete_item = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        delete_item.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
        return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/item.html", context)


def update_item(request, id):
    update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update)
        if form.is_valid():
            todo_task = form.save()
            todo_task.save()
            return redirect("todo_list_detail", id=todo_task.list.id)
    else:
        form = TodoItemForm(instance=update)
    context = {
        "update": update,
        "form": form,
    }
    return render(request, "todos/update.html", context)
